EXP_SUM
=======

This directory contains 3 subdirectories of numerical results for the approximation of different functions 

- directory `1_x` for $\frac{1}{x}$ (absolute error)
- directory `1_x_rel` for $\frac{1}{x}$ (relative error)
- directory `1_sqrtx` for $\frac{1}{\sqrt(x)}$
- directory `sqrtx` for $\sqrt(x)$
  
by sums of exponentials. Results are summarised
in each directory in the respective file "tabelle". 
