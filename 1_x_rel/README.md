Relative Best Approximations of $x^{-1}$ by Exponential Sums
============================================================

For explanations see:

1. Exponential Sums - Definition and Properties (in `Def.pdf`)
2. Tables - Description of the Data Files (in `Tables.pdf`)
3. Accuracy (in `Accuracy.pdf`)
4. Description of the Program (in `ProgramDescription.pdf`)

Pascal program:  `ExpOptRel.pas`

Data files: `Rel1_x_n.zip`
