program ExpOptimierung; {fpc ExpOptRel}
{relative best approximation of 1/x by exponential sums}

const NFeld=70; const NNFeld=140;

type T_Koeff = array [1..NFeld,1..2] of Extended;
type T_Vektor = array [1..NNFeld] of Extended;
type T_Matrix = array [1..NNFeld,1..NNFeld] of Extended;

var i,j,k,n,Fall: integer;
var AB: T_Koeff;
var XX,XI,F,FF,DF: T_Vektor;
var Kommentar: Boolean;
var omega,b,bu,AA,op,ApprFehler,Adiff,tau: Extended;
var name: string;
label 1,2,3,4,5,6,7;

function CollectSISS(s1: string; i: integer; s2,s3: string): string;
var t: text;
begin assign(t,'scratch'); rewrite(t); writeln(t,s1,i:1,s2,s3); close(t);
   reset(t); readln(t,s1); close(t);
   CollectSISS:=s1
end;

function Zerlege(x: Extended): string;
const eps = 1E-16;
var t: text; xx: Extended; k,ex,m: integer; s: string; label 1;
begin assign(t,'scratch'); xx:=x; k:=0;
1: if xx<10 then begin m:=round(xx+eps); ex:=k end
   else	begin xx:=(xx+eps)/10; k:=k+1; goto 1 end;
   rewrite(t); writeln(t,m:1,'E',ex:1); close(t);
   reset(t); readln(t,s); close(t);
   Zerlege:=s
end;


{Allgemeine lineare Algebra ----------------------------------------}

function GL_Loesen(n: integer;
		   var A: T_Matrix; var B_,X: T_Vektor): Boolean;
const eps = 1E-16;
var i,j,k: integer; s,p: Extended; A_: T_Matrix; b: T_Vektor; BB: Boolean;
label 9;
begin {Äquilibrieren} B:=B_; BB:=true;
   if Kommentar then writeln('*** GL_Loesen Start ***'); A_:=A;
   for i:=1 to n do
   begin s:=0; for j:=1 to n do s:=s+sqr(A[i,j]);
      if s=0 then
      begin writeln('GL_Loesen: ',i:1,'. Zeile = 0 ### Abbruch');
	 BB:=false; goto 9
      end;
      b[i]:=b[i]/s; for j:=1 to n do A[i,j]:=A[i,j]/s
   end;
   for i:=1 to n do
   begin {Pivotwahl} p:=A[i,i]; k:=i;
      for j:=i+1 to n do
	 begin s:=A[j,i]; IF abs(s)>abs(p) then begin p:=s; k:=j end end;
	 IF k>i then
	 begin for j:=i to n do
	    begin s:=A[i,j]; A[i,j]:=A[k,j]; A[k,j]:=s end;
            s:=b[i]; b[i]:=b[k]; b[k]:=s
	 end; {Austausch}
	 if p=0 then
	 begin write('GL_Loesen: ',i:1,'-tes Pivotelement = 0 ### Abbruch');
	    BB:=false; goto 9
	 end;
         if Kommentar then if abs(p)<1E-10 then
	    writeln('### GL_Loesen: ',i:1,'-tes Pivotelement klein: ',p);
         if abs(p)<eps then
	 begin write(' ### GL_Loesen: ',i:1,'-tes Pivotelement<eps');
	    BB:=false; goto 9
	 end;
         {A[i,i]:=1;}
	 b[i]:=b[i]/p; for j:=i+1 to n do A[i,j]:=A[i,j]/p;
	 for k:=i+1 to n do
	 begin s:=A[k,i];
	    {A[k,i]:=0;}
	    if s<>0 then
            begin b[k]:=b[k]-s*b[i];
                  for j:=i+1 to n do A[k,j]:=A[k,j]-s*A[i,j]
         end end
      end {obere Dreiecksform};
      for i:=n downto 1 do
      begin s:=b[i];
	 for j:=i+1 to n do s:=s-A[i,j]*X[j]; X[i]:=s
      end;
9: GL_Loesen:=BB;
   if not BB then
   begin write(' ### GL_Lösen: Abbruch'); A:=A_; 
      if Kommentar then
      begin writeln('### GL_Lösen: Ausgabe der Funktionalmatrix:');
	 for i:=1 to n do
	 begin writeln('Zeile i=',i:1);
	    for j:=1 to n do writeln(j:1,' ',A[i,j])
   end end end;
{   for i:=1 to nnfeld do
   begin writeln('Zeile i=',i:1);
      for j:=1 to nnfeld do writeln(j:1,' ',A[i,j])
   end;
   for i:=1 to nnfeld do
   begin writeln('B_ und X: ',i:1,' ',B_[i],' ',X[i]) end;}
   if Kommentar then writeln('*** GL_Loesen Ende *** ',BB)
end; {Lösung des linearen Gleichungssystems}

{zur rationalen Funktion -------------------------------------------}
   
function ExpSum(n : integer; var AB: T_Koeff; x: Extended): Extended;
var f: Extended; var j: integer;
begin f:=0;
   for j:=1 to n do f:=f+AB[j,1]*exp(-AB[j,2]*x);
   ExpSum:=f
end; {Auswertung der rationalen Funktion mit Koeffizienten AB bei x}

procedure AusAB(n: integer; var AB: T_Koeff);
var i: integer;
begin writeln('Output of the coefficients AB of the exponential sum');
   writeln('   Degree n =',n:2);
   for i:=1 to n do
      writeln('   a[',i:1,']=',AB[i,1],'   b[',i:1,']=',AB[i,2])
end;

procedure AusXI(n: integer; var XI: T_Vektor);
var i: integer;
begin writeln('Ausgabe der Interpolationstellen der Exponentialsumme');
   for i:=1 to 2*n do writeln('   Xi[',i:1,']=',XI[i])
end;

function ExpDefekt(n: integer; var AB: T_Koeff; var XI,F: T_Vektor): Extended;
var i: integer; a,b: Extended;
begin a:=0; for i:=1 to 2*n do
   begin b:=ExpSum(n,AB,XI[i])-F[i]; b:=abs(b); if b>a then a:=b
   end;
   ExpDefekt:=a
end; {maximaler Interpolationsfehler}

function AusDefekt(n: integer; Details: Boolean;
		   var AB: T_Koeff; var XI,F: T_Vektor): Extended;
var i: integer; a,b: Extended;
begin a:=0; write('Interpolationsfehler: '); if Details then writeln;
   for i:=1 to 2*n do
   begin b:=ExpSum(n,AB,XI[i])-F[i];
      if Details then writeln('i=',i:1,' : r(x_i)-f_i = ',b);
      b:=abs(b); if b>a then a:=b
   end;
   writeln('maximale Abweichung: ',a);
   AusDefekt:=a
end; {Ausgabe der Interpolationsfehler, Rückgabe: maximaler Fehler}

procedure AusAlles(n: integer; R: Extended; var AB: T_Koeff;
		   var XI,F: T_Vektor);
begin writeln('Optimierungsintervall: [1,',R,']');
   AusAB(n,AB); AusXI(n,XI); AusDefekt(n,false,AB,XI,F)
end;

{Ableitungen, Funktionalmatrix}

function AblExpA(i,j: integer; var AB: T_Koeff; var XI: T_Vektor): Extended;
begin AblExpA:=exp(-AB[j,2]*XI[i])
end; {Ableitung der Exponentialsumme nach AB[j,1] bei x=XI[i]}

function AblExpB(i,j: integer; var AB: T_Koeff; var XI: T_Vektor): Extended;
begin AblExpB:=-AB[j,1]*XI[i]*exp(-AB[j,2]*XI[i])
end; {Ableitung der Exponentialsumme nach AB[j,2] bei x=XI[i]}

procedure AblExp(n: integer; var AB: T_Koeff; var XI: T_Vektor;
		 var FAbl: T_Matrix);
var i,j: integer;
begin
   for i:=1 to 2*n do for j:=1 to n do
   begin FAbl[i,j]:=AblExpA(i,j,AB,XI);
       FAbl[i,j+n]:=AblExpB(i,j,AB,XI)
end end; {FAbl=Funktionalmatrix: Exponentialsumme nach Koeffizienten}

{Newton-Verfahren für Berechnung der Koeffizienten der interpolierenden Exponentialsumme}

procedure NewtExp(n: integer; var AB: T_Koeff; var XI,F: T_Vektor);
{Newton method for interpolation by exponential sums}
{AB alte Koeffizienten bei Eingabe - Ausgabe: neue Koeffizienten}
var FAbl     : T_Matrix;
   RS,XX     : T_Vektor;
   ABalt,DAB : T_Koeff;
var i,error: integer; omega: Extended; label 0,1,2,3,4;
begin
0: for i:=1 to n do
   begin ABalt[i,1]:=AB[i,1]; ABalt[i,2]:=AB[i,2] end;
   AblExp(n,ABalt,XI,FAbl); {Funktionalmatrix berechnet}
   for i:=1 to 2*n do RS[i]:=ExpSum(n,ABalt,XI[i])-F[i]; {Residuum}
   if not GL_Loesen(2*n,FAbl,RS,XX) then goto 4;
   {Lösung des Gleichungssystems} 
   for i:=1 to n do
   begin DAB[i,1]:=XX[i]; DAB[i,2]:=XX[i+n] end;
   {Newton-Korrektur zu ABalt}
   omega:=1;
1: for i:=1 to n do
   begin AB[i,1]:=ABalt[i,1]-omega*DAB[i,1];
         AB[i,2]:=ABalt[i,2]-omega*DAB[i,2]
   end;
   for i:=1 to n do {Prüfung}
   begin if AB[i,1]<=0 then begin error:=i; goto 2 end;
      error:=-i;
      if i=1 then if AB[i,2]<=0 then goto 2;
      if i>1 then if AB[i,2]<=AB[i-1,2] then goto 2
   end;
   goto 3;
2: if Kommentar then
   begin writeln('NewtExp: error=',error);
      writeln('NewtExp: omega=',omega,' nicht ausreichend')
   end;
   omega:=omega/2; if omega>0.0001 then goto 1;
3: if ExpDefekt(n,AB,XI,F)>1E-13 then goto 0;
4: end; {gedämpftes Newton-Verfahren}

{Remez ------------------------------------------------}
function phi(x: Extended): Extended;
begin phi:=1/x end;

function Err(n: integer; x: Extended; var AB: T_Koeff): Extended;
var pp: Extended;
begin pp:=phi(x); Err:=(pp-ExpSum(n,AB,x))/(pp+tau*(1-pp)) end;

procedure Extremum(n: integer; a,b: Extended; var AB: T_Koeff;
		   var x,y: Extended);
{Die Fehlerfunktion Err hat in [a,b] bei x das Extremum y}
const M	= 10;
var xx,h,xm,ym,xmalt,yl,yr,yy: Extended; i,im: integer; label 1,9;
begin h:=(b-a)/M; ym:=0;
if Kommentar then writeln('####### Aufruf Extremum1 mit a=',a,', b=',b);
   for i:=0 to M do
   begin xx:=b-i*h; yy:=Err(n,xx,AB);
      if abs(yy)>abs(ym) then begin xm:=xx; ym:=yy; im:=i end
   end;
   if Kommentar then
   begin for i:=0 to M do writeln('# Extr. - Gitter: x=',a+i*h,' Err=',Err(n,a+i*h,AB));
      writeln('####### Aufruf Extremum2: diskr. Extr.',ym,' bei x=',xm);
   end;
   if im=0 then if abs(Err(n,xm+h,AB))>abs(Err(n,xm,AB)) then
   begin if Kommentar then writeln('Fall > xm=',xm);
      if xm=b then goto 9
   end;
1: xmalt:=xm; h:=h/2;
   if Kommentar then writeln('####### Aufruf Extremum3: h=',h);
   yl:=Err(n,xm-h,AB); ym:=Err(n,xm,AB); yr:=Err(n,xm+h,AB);
   yy:=2*(yl+yr-2*ym);
   if abs(yy)>1E-15 then xm:=xm+h*(yl-yr)/yy;
   if Kommentar then
   begin writeln('####### Aufruf Extremum4, x=',xm,' Nenner=',yy);
      yl:=Err(n,xm-h,AB); ym:=Err(n,xm,AB); yr:=Err(n,xm+h,AB);
      writeln('## yl=',yl,', ym=',ym,' yr=',yr)
   end;
   if abs(xm-xmalt)<(b-a)*1E-17 then goto 9;
   if xm>b then begin xm:=b; goto 9 end;
   if xm<a then begin xm:=a; goto 9 end;
   goto 1;
   if x>b then x:=b; if x<a then x:=a;
9: x:=xm; y:=Err(n,x,AB);
   if Kommentar then
   begin writeln('####### Aufruf Extremum5, x=',x,', y=',y); readln
   end
   {;kommentar:=false}
end;

procedure Alternanten(n: integer; R: Extended;
	  var AB: T_Koeff; var XI,XX,FF,DFF: T_Vektor; var Adiff: Extended);
{Alternantenwerte auf XX[i],FF[i] für 1<=i<=2n+1; 
DFF: Defekt der Alternantenfunktion, Adiff: Abweichung der Alternantenwerte}
var i: integer; x,y,amin,amax: Extended;
begin FF[1]:=Err(n,1,AB); XX[1]:=1; x:=1; XI[2*n+1]:=R;
    amin:=abs(FF[1]); amax:=amin;
    for i:=2 to 2*n+1 do
    begin Extremum(n,XI[i-1],XI[i],AB,x,y); FF[i]:=y; XX[i]:=x;
       if abs(y)>amax then amax:=abs(y); if abs(y)<amin then amin:=abs(y);
       if Kommentar then writeln('Alternante zw. XI[',i-1:1,'] und XI[',i:1,
				 '] bei x=',x,' mit y=',y)
    end;
    if Kommentar then
    begin writeln('####### Aufruf Alternanten1, FF=');
       for i:=1 to 2*n+1 do writeln(i,' ',FF[i])
    end;
    for i:=1 to 2*n do DFF[i]:=FF[i]+FF[i+1];
    if Kommentar then
    begin writeln('####### Aufruf Alternanten2, Differenzen DFF=');
       for i:=1 to 2*n do writeln(i,' ',DFF[i])
    end;
    Adiff:=amax-amin
end; {FF: Ordinatenwerte der Alternante}

function AuswertungFF(n: integer; FF: T_Vektor): Extended;
var s: Extended; i: integer;
begin s:=0; for i:=1 to 2*n do if s<abs(FF[i]) then s:=abs(FF[i]);
   AuswertungFF:=s;
end;

{dividierte Differenzen für F aus Alternante}

procedure DivDifGrad(R: Extended; n,j: integer;
		     var AB: T_Koeff; var XI,DF,G: T_Vektor);
{G[i] = Ableitung von F[i] nach XI[j], DF: Defekte der Basislösung}
const eps = 1E-2;{1E-6}
var XX,XIneu,F,FF,DFF: T_Vektor; ABneu: T_Koeff; d: Extended; k: integer;
begin
   if j=1 then d:=XI[1]-1 else d:=XI[j]-XI[j-1]; d:=eps*d;
   if Kommentar then
      begin writeln('###DivDifGrad d=',d,'  ### alte Alternante: ');
      for k:=1 to 2*n do writeln(k,' ',DF[k])
      end;
   XIneu:=XI; XIneu[j]:=XI[j]+d; ABneu:=AB;
   for k:=1 to 2*n do F[k]:=phi(XIneu[k]);
   NewtExp(n,ABneu,XIneu,F);
   if Kommentar then begin AusAlles(n,R,ABneu,XIneu,F); readln end;
   Alternanten(n,R,ABneu,XIneu,XX,FF,DFF,Adiff);
   if Kommentar then
   begin writeln('####DivDifGrad neue Alternante: ');
      for k:=1 to 2*n do writeln(k,' ',DFF[k])
   end;
   for k:=1 to 2*n do G[k]:=(DFF[k]-DF[k])/d
end;

procedure DivDifF(R: Extended; n: integer;
		  var AB: T_Koeff; var XI,DF: T_Vektor; var FM: T_Matrix);
{FM = Funktionalmatrix F nach XI}
var j,k: integer; G: T_Vektor;
begin for j:=1 to 2*n do
   begin DivDifGrad(R,n,j,AB,XI,DF,G);
      for k:=1 to 2*n do FM[k,j]:=G[k];
      if Kommentar then for k:=1 to 2*n do writeln(j:3,k:3,' ',G[k])
   end;
end;
{--------------------------------------}

procedure EinOptX(var n: integer; d: string; var R: Extended;
		  var AB: T_Koeff; var XI: T_Vektor;
                  var ApprFehler, Adiff: Extended);
var i: integer; t: text;
begin assign(t,d); reset(t); readln(t); readln(t,n); readln(t,R);
   for i:=1 to n do readln(t,AB[i,1]);
   for i:=1 to n do readln(t,AB[i,2]);
   for i:=1 to 2*n do readln(t,XI[i]);
   readln(t,ApprFehler); readln(t,Adiff); close(t);
   writeln('Data read from the file >',d,'<')
end;

procedure AusOptX(var n: integer; d: string; var R: Extended;
		  var AB: T_Koeff; var XI,XX,FF: T_Vektor;
                  ApprFehler: Extended);
var i: integer; t: text;
begin assign(t,d); rewrite(t);
   writeln(t,'optimal approximation of 1/x by sum[1<=k<=n] a[k]*exp(-b[k]*x) in [1,R] (relative error)');
   writeln(t,n,'  degree = number n of terms');
   writeln(t,R,'  1.0     R and gamma=1 for 1/x');
   for i:=1 to n do writeln(t,AB[i,1],'   coefficient a[',i:1,']');
   for i:=1 to n do writeln(t,AB[i,2],'   coefficient b[',i:1,']');
   for i:=1 to 2*n do writeln(t,XI[i],'   interpolation point xi[',i:1,']');
   writeln(t,ApprFehler,'  relative approximation error');
   Alternanten(n,b,AB,XI,XX,FF,DF,Adiff);
   writeln(t,Adiff,'  variation of alternant');
   for i:=1 to 2*n+1 do
      writeln(t,XX[i],' ',FF[i],' x,y of alternant ',i:1);
   close(t);
   writeln('data written onto >',d,'<');
end; {AusOptX}

procedure GS_lokal(n,i: integer; R: Extended; var AB: T_Koeff;
		   var XI,F: T_Vektor);
var XX,FF,DF: T_Vektor;
begin Alternanten(n,R,AB,XI,XX,FF,DF,Adiff);
   DivDifGrad(R,n,i,AB,XI,DF,XX);
   XI[i]:=XI[i]-DF[i]/XX[i]; F[i]:=phi(XI[i]); NewtExp(n,AB,XI,F)
end; {GS_lokal}

procedure GS(n: integer; R: Extended; var AB: T_Koeff;
	     var XI,F: T_Vektor);
var i: integer;
begin for i:=1 to 2*n do F[i]:=phi(XI[i]);
   for i:=2*n downto 1 do GS_lokal(n,i,R,AB,XI,F);
   for i:=2 to 2*n do GS_lokal(n,i,R,AB,XI,F)   
end; {GS}

function NewtBest(n: integer; R: Extended; var AB: T_Koeff;
		  var XI: T_Vektor; Anzahlmax: integer;
		  var ApprFehler,Adiff: Extended): integer;
const Genau = 1E-17; SehrGenau = 4E-17; Anzahlmin = 20;
var Erfolg,i,iter: integer; AAalt,omega,tn,ta: Extended;
    XIalt,XX,F,FF,DF,DX: T_Vektor; FM: T_Matrix;
label 2,4,5,9;
begin Erfolg:=1; iter:=1; Adiff:=999; write('### Call of NewtBest');
   omega:= 1; 
5: writeln; write(' # iter=',iter:1); XIalt:=XI; 
   for i:=1 to 2*n do F[i]:=phi(XI[i]); NewtExp(n,AB,XI,F);
   Alternanten(n,R,AB,XI,XX,FF,DF,Adiff);
   AAalt:= Adiff; write(' # defect =',AAalt);
   if (AAalt<=SehrGenau) and (iter>1) then
   begin Erfolg:=2; goto 9 end;
   DivDifF(R,n,AB,XI,DF,FM);
   if not GL_Loesen(2*n,FM,DF,DX) then
      begin omega:=0; write(' # GL'); goto 4 end;
   if Kommentar then
   begin writeln('#### XI-correction:');
      for i:=1 to 2*n do writeln(i:1,' ',DX[i])
   end;
2: {omega-Schleife}
   XI:=XIalt; write(' # omega=',omega);
   for i:=1 to 2*n do XI[i]:=XIalt[i]-omega*DX[i];
   {Prüfung - rechter Rand}
   if Kommentar then write(' # Rechts: ta=',ta:1:3,' tn=',tn:1:3);
   if Kommentar then
   begin writeln('#### omega=',omega);
      for i:=1 to 2*n do writeln(i:1,' ',XI[i]); readln
   end;
   {linker Rand}
   if XI[1]<1 then
   begin write(' # XI[1]<1');
4:    if omega<1E-12 then
      begin write(' # omega too small'); XI:=XIalt; Erfolg:=0; goto 9
      end;                                   
      omega:=omega/2; goto 2
   end;                                      
   {Anordnung}
   for i:=2 to 2*n-1 do
   begin tn:=(XI[i]-XI[i-1])/(XI[i+1]-XI[i-1]);
      if tn<=0 then begin write(' # tn<=0 for i=',i:1); goto 4 end;
      if tn>=1 then begin write(' # tn>=0 for i=',i:1); goto 4 end;
      ta:=(XIalt[i]-XIalt[i-1])/(XIalt[i+1]-XIalt[i-1]);
      if abs(ta-tn)>0.01{0.001} then
      begin write(' # |ta-tn|>0.01 for i=',i:1); goto 4 end;
      if Kommentar then write(' # ta=',ta:1:3,' tn=',tn:1:3);
   end; 
   {Interpolation an neuen Stellen}
   if Kommentar then writeln('Interpolation an neuen Stellen');
   for i:=1 to 2*n do F[i]:=phi(XI[i]);  
   NewtExp(n,AB,XI,F);                   
   Alternanten(n,R,AB,XI,XX,FF,DF,Adiff);
   ApprFehler:=AuswertungFF(n,FF);
   if Adiff>=AAalt then
      begin if Kommentar then writeln('neuer Defekt ',Adiff,' >= ',AAalt);
	 write(' # error enlarged');
	 goto 4
      end;
   if Kommentar then writeln('Defekte: alt = ',AAalt,', neu = ',Adiff);
   {erfolgreiche Korrektur}
   AAalt:=Adiff;
   write(' # improvement');
   if Adiff<=SehrGenau then begin Erfolg:=2; goto 9 end;
   {if iter<Anzahlmin then begin iter:=iter+1; goto 5 end;}
   iter:=iter+1; omega:=1;{if omega<0.50001 then omega:=2*omega;}
   if iter<=Anzahlmax then goto 5; write(' # iter=Anzahlmax=',Anzahlmax:1);
9: NewtBest:=Erfolg; writeln(' ### NewtBest: ',Erfolg:1);
   {Ausgabe an Zwischendatei:}
   if Erfolg>0 then AusOptX(n,'Zw',R,AB,XI,XX,FF,ApprFehler)
end; {NewtBest}

function bAendern(n: integer; var AB: T_Koeff; var XI: T_Vektor;
	 var R: Extended; Rdest: Extended;
	 var ApprFehler,AlternFehler: Extended; t: Extended): integer;
const Iterationsanzahl = 1000;
   {0<t<1 verkleinert, t>1 vergrößert. - Rdest Zielgröße, falls <>0}
begin R:=XI[2*n]+t*(R-XI[2*n]); if Rdest<>0 then
   begin if (t<1) and (Rdest>R) then R:=Rdest else
      if (t>1) and (Rdest<R) then R:=Rdest
   end; writeln('New R =',R);
   bAendern:=NewtBest(n,R,AB,XI,Iterationsanzahl,ApprFehler,AlternFehler);
end;

begin {Initialisierung}
   Kommentar:=false; Adiff:=998; ApprFehler:=998; tau:=0;
   for i:=1 to NFeld do begin AB[i,1]:=0; AB[i,2]:=0 end;
   for i:=1 to NNFeld do DF[i]:=0;
   {Exponentialfunktion und Interpolationspunkte als Start einlesen}
   EinOptX(n,'rel',b,AB,XI,ApprFehler,Adiff);
1: writeln('#######################################################');
   for i:=1 to 2*n do F[i]:=phi(XI[i]);
   writeln('Parameters: degree = ',n:1);
   writeln('  interval:                [1, ',b,' ]');
   writeln('  last interpolation point:    ',XI[2*n]);
   for i:=1 to 2*n do FF[i]:=F[i]-ExpSum(n,AB,XI[i]);
   omega:=AuswertungFF(n,FF);
   writeln('  error of interpolation:      ',omega);
   Alternanten(n,b,AB,XI,XX,FF,DF,Adiff);
   writeln('  equi-oscillation variation:  ',Adiff);
   ApprFehler:=AuswertungFF(n,FF);
   writeln('  relative approximation error:',ApprFehler);
   if (omega<1E-12) and (Adiff<1E-14) then
      AusOptX(n,'rel',b,AB,XI,XX,FF,ApprFehler);
   writeln('#######################################################');
   writeln('Options: 0: end');
   writeln('1: Newton method');
   writeln('2: show parameters');
   writeln('3: in/output');
   writeln('4: change parameters');
   writeln('5: Gauss-Seidel step');
   readln(Fall);

   if Fall=1 then
   begin write('1: interpolation / 2: best-approximation / ',
	       '-k: k-times GS and Newton ... ');
      readln(Fall);
      if Fall=1 then {Interpolieren}
      begin NewtExp(n,AB,XI,F); goto 1 end;
      if Fall>1 then {Newton-Schritt für Bestapproximation}
      begin Fall:=NewtBest(n,b,AB,XI,Fall-1,ApprFehler,Adiff);
7:	 if Fall=2 then writeln('Newton for best-approximation successful');
	 if Fall=1 then writeln('Newton for best-approximation: improvement possible');
	 if Fall=0 then writeln('Newton for best-approximation failed');
	 goto 1
      end;
      {Fall<0} k:=-Fall;
      for i:=1 to k do
      begin GS(n,b,AB,XI,F);
	 Fall:=NewtBest(n,b,AB,XI,1,ApprFehler,Adiff)
	 end;
      goto 1 
   end;

   if Fall=2 then {Anzeige}
   begin writeln('1: alternant / 2: coeff. AB / 3: interp.points XI / ');
      write('4: values in the interval [XI[i],XI[i+1]] ... ');
      readln(Fall); if (Fall<1) or (Fall>4) then goto 1;
      if Fall=1 then {Alternante}
      begin Alternanten(n,b,AB,XI,XX,FF,DF,Adiff);
	 writeln('alternant:'); op:=0;
	 for i:=1 to 2*n+1 do
	 begin writeln('i = ',i:2,'  x = ',XX[i],', y = ',FF[i]);
	    if abs(FF[i])>op then op:=abs(FF[i])
	 end;
	 writeln('approximation error   =',op); ApprFehler:=op;
	 writeln('equioscillation variation =',Adiff);
	 goto 1
      end;
      if Fall=2 then begin AusAB(n,AB); goto 1 end;
      if Fall=3 then begin AusXI(n,XI); goto 1 end;
      {Fall=4} write('i from 0,...,2n: ... '); readln(i);
      if (i<0) or (i>2*n) then begin writeln('wrong choice'); goto 1 end;
      write('number of intermediate values ... '); readln(Fall);
      if Fall<=0 then begin writeln('wrong choice'); goto 1 end;
      if i=0 then AA:=1 else AA:=XI[i];
      if i<2*n then omega:=XI[i+1]-AA else omega:=b-AA; omega:=omega/Fall;
      for j:=0 to Fall do
      begin bu:=AA+j*omega; writeln('x=',bu,' error=',Err(n,bu,AB)) end;
      goto 1
   end;
   
   if Fall=3 then {Ein/Ausgabe}
   begin name:=CollectSISS('Rel1_x_n',n,'.',Zerlege(b));
      write('1: read / 2: output to ',name,' / 3: output to ... ');
      readln(Fall); if (Fall<1) or (Fall>3) then goto 1;
      if Fall=1 then {Einlesen}
      begin write('read from ... '); readln(name);
	 if name='' then name:='rel';
	 EinOptX(n,name,b,AB,XI,ApprFehler,Adiff); goto 1
      end;
      if Fall=2 then
5:    begin AusOptX(n,name,b,AB,XI,XX,FF,ApprFehler); goto 1 end;
      {Abspeichern}
      write('output onto ... '); readln(name);
      if name='' then name:='rel'; goto 5
   end;
   
   if Fall=4 then {Parameter ändern}
   begin writeln('1: xi local / 2: R:=... / 3: change xi,R by factor /');
      write('              4: change R / 5: iteratively / 6: tau ... ');
      readln(Fall); ApprFehler:=999; Adiff:=999;
      if (Fall<1) or (Fall>6) then goto 1;
      if Fall=1 then {XI lokal ändern}
      begin 
6:	 write('XI[i] for i = ... '); readln(i);
	 if i<1 then goto 1;
	 if i>2*n then begin writeln('i>2n'); goto 6 end;
	 writeln('old value: ',XI[i]);
	 write('1: explicit / 2: local Newton step '); readln(Fall);
	 if Fall<0 then goto 1 else
	 if Fall=1 then
	    begin write('new XI[i] = ... '); readln(XI[i]); goto 1 end;
	 GS_lokal(n,i,b,AB,XI,F);
	 writeln('new XI[i] = ',XI[i]); goto 1
      end;
      if Fall=2 then {b ändern}
      begin
3:	 write('new value of R = ... '); readln(omega);
	 if omega<0 then goto 1;
	 if omega<XI[2*n] then
	 begin writeln('value too small: below XI[2*n]=',XI[2*n]); goto 3 end
	 else b:=omega; goto 1
      end;
      if Fall=3 then {xi-Werte und b reduzieren}
      begin write('factor = ... '); readln(omega);
	 if omega<0 then goto 1;
	 for i:=1 to 2*n do XI[i]:=1+omega*(XI[i]-1);
	 b:=1+omega*(b-1); goto 1
      end;
      if Fall=4 then
      begin write('Rnew := XI[2*n] + t*(R-XI[2*n]) with t = ... ');
	 readln(omega); if omega<0 then goto 1;
	 Fall:=bAendern(n,AB,XI,b,0,ApprFehler,Adiff,omega);
	 writeln('Success of RChange = ',Fall:1); goto 1
      end;
      if Fall=5 then
      begin
2:	 write('Rnew := XI[2*n] + t*(R-XI[2*n]) mit t ='); readln(omega);
	 if omega<=0 then begin writeln('t>0 required'); goto 2 end;
	 if omega=1 then begin writeln('t=1 is useless'); goto 2 end;
	 write('target R ... '); readln(bu);
	 if (omega<1) and (bu>b) then
	 begin writeln('target value < R required'); goto 2 end;
	 if (omega>1) and (bu<b) then
	 begin writeln('target value > R required'); goto 2 end;
4:	 Fall:=bAendern(n,AB,XI,b,bu,ApprFehler,Adiff,omega);
	 write('*** R=',b,'  success=',Fall); readln;
	 if (Fall<2) or ((b<=bu) and (omega<1)) or ((b>=bu) and (omega>1)) then
	 begin writeln('end of bChange with Rlow=',bu);
	    writeln('  R=',b,', Success=',Fall); goto 1
	 end;
	 goto 4
      end;
      {Fall=6}
      write('previous value of tau =',tau,' - New value ... '); readln(tau);
      goto 1
   end;

   if Fall=5 then
   begin GS(n,b,AB,XI,F); goto 1 end;

   if Fall<>0 then goto 1;

   writeln('*** end ***')
end.

